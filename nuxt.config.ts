// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },

  // Dev Extends
  // extends: [
  //   '../../../Packages/IICL/iicl',
  //   '../../Stores APIs/inkb-stores-apis',
  // ],

  // Prod Extends
  extends: ['github:InkingBird/inkb-stores-apis', 'github:InkingBird/iicl'],

  app: {
    head: {
      title: 'InkingBird Stores',
      htmlAttrs: {
        lang: 'en',
      },
      link: [
        {
          rel: 'stylesheet',
          href: 'https://cdn.jsdelivr.net/gh/iconoir-icons/iconoir@main/css/iconoir.css',
        },
      ],
    },

    rootTag: 'main',
  },

  // modules: ["@nuxtjs/tailwindcss"],
  modules: ['@nuxtjs/supabase', '@nuxt/image', '@pinia/nuxt'],

  supabase: {
    cookieName: 'ib',
    redirect: false,
  },
});
