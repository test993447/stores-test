/* eslint-disable camelcase */
import type {
  stores,
  menu,
  countries,
  currencies,
  prices,
  products,
  product_tags,
} from '@prisma/client';

export type Store = stores;

export type StoreWithMenuAndExtra = stores & {
  cover_url: string;
  icon_url: string;
  fixedRating: number;
  countries: countries;
  currencies: currencies;
  menu: menu[];
  for_show_case: menu[];
};

export type StoreMenu = menu;

export type ProductWithPrice = products & {
  price_products_priceToprice: prices & {
    currencies_prices_currencyTocurrencies: currencies;
  };
};

export type ProductWithPriceStoreMenu = ProductWithPrice & {
  menu: menu;
  p_thumb_url: string;
  fixedRating: number;
  stores_products_store_at_addressTostores: Store;
  tags: product_tags[];
};

export type MenuProducts = { menu: menu } & { products: products[] };
