import { defineStore } from 'pinia';
import type { ProductWithPriceStoreMenu } from '~/types/stores';

export const useProductStore = defineStore('Product', {
  state: () => {
    return {
      product: null as ProductWithPriceStoreMenu | null,
      currentProduct: null as ProductWithPriceStoreMenu | null,
    };
  },
  getters: {
    getProduct: (state) => state.product,
    getCurrentProduct: (state) => state.product,
  },
  actions: {
    setProduct(store: ProductWithPriceStoreMenu | null) {
      if (store) this.product = store;
    },
    setCurrentProduct(store: ProductWithPriceStoreMenu | null) {
      if (store) this.product = store;
    },
  },
});
