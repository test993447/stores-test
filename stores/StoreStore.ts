import { defineStore } from 'pinia';
import type { StoreMenu, StoreWithMenuAndExtra, Store } from '~/types/stores';

export const useStoreStore = defineStore('Store', {
  state: () => {
    return {
      store: null as StoreWithMenuAndExtra | Store | null,
      currentStore: null as StoreWithMenuAndExtra | Store | null,
      currentlyViewedMenu: null as StoreMenu | null,
    };
  },
  getters: {
    getStore: (state) => state.store,
    getCurrentStore: (state) => state.currentStore,
    getcurrentlyViewedMenu: (state) => state.currentlyViewedMenu,
  },
  actions: {
    setStore(store: StoreWithMenuAndExtra | Store | null) {
      if (store) this.store = store;
    },
    setCurrentStore(store: StoreWithMenuAndExtra | Store | null) {
      if (store) this.currentStore = store;
    },
    setMenu(menu: StoreMenu | null) {
      if (menu) this.currentlyViewedMenu = menu;
    },
  },
});
