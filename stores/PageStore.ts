import { defineStore } from 'pinia';

export const usePageStore = defineStore('Page', {
  state: () => {
    return {
      isLoading: false as boolean,
    };
  },
  getters: {
    getLoading: (state) => state.isLoading,
  },
  actions: {
    setLoading(loading: boolean) {
      if (loading) this.isLoading = loading;
    },
  },
});
